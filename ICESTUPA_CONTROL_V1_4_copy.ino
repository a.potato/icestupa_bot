/******************************************************

*********** Leer comentarios ***********

  Alen Ingeniería SpA
  Lógica Funcionamiento Sistema de Automatización ICESTUPA
  Autor:  José Mardones
          Enzo Alegría
          Anton Potapov

  Versión: 1.4
  Fecha: 19 de agosto 2023

  Lógica de funcionamiento viene dado por el archivo alojado en el siguiente link

  https://alensolucionescl.sharepoint.com/:f:/s/proyectos/EgLsjEgY2wZApaymUv_hNR4BMrtrj28XLstyAGEO7g-VlQ?e=G4hoVy

  Cliente: Nilus

  Comentarios: 1.- Integración de sensores y pruebas de funcionamiento
             2.- Comprobación de secuencia de funcionamiento de acuerdo a la respuesta de la electroválvula
             3.- Reprogramación de secuencia optimizando su funcionamiento.
             4.- Se quita modo 1, quedando solo mode 0 y mode 2, incorportando la función de drenaje() dentro de las acciones de vega()
             5.- Se agrega funciones de promedio temporal y control por histeresis



*/

//Iniciación de sensores de Arudino
#include <Arduino_HTS221.h>
float Temp_Int;
float Hum_Int;

//Iniciación de termoculpa exterior
#include <Adafruit_MAX31865.h>
#define RREF 430.0
#define RNOMINAL 100.0
// Use software SPI: CS, DI, DO, CLK
int CS2 = 9;
Adafruit_MAX31865 thermo = Adafruit_MAX31865(CS2, 11, 12, 13);
float Temp_Ext;

//Parametros y libreiras para usar SD
#include <SPI.h>
#include <SD.h>
const int CS1 = 10;


//Valores de referencia de cambio de estados
//Temperatura de cambio de estado real
int Temp_Ref = 25;
float temp_average_vector[10];
float Temp_treshold=1;

// Tiempo de vaciado de linea de agua
int Time_Vacio = 10000;

const int ledPin = 13;

//Puertos de control de giros
const int CCW = 2;  //Sentido antihorario o giro derecho
const int CW = 3;   //Sentido horario o giro izquierdo

//Estados de iniciales de giro
int CCW_State = 0;
int CW_State = 0;
//Puertos de medicion de activación de giros

int CCW_Active = A3;
int CW_Active = A4;
int Spin_Active = A5;

//Sensor de Voltaje
int S_Volt = A1;
float V_Ref = 11.1; //12 voltaje
float V_Med = 0;
float V_treshold=0.3;
float V_average_vector[10];
bool V_triggered = false;


//Pulsador
int P = 8;

//Referencias de mediciones con giros detenidos
int CCW_Ref0 = 0;
int CW_Ref0 = 0;
int Spin_Ref0 = 0;

//Referencias de mediciones con giros activados
int CCW_Ref1 = 0;
int CW_Ref1 = 0;
int Spin_Ref1 = 0;

//Modo en función de estado de posición de valvula Mode 0: Asperción; Mode 1: Drenaje; Mode 2:Vega
int Mode = 0;

//Variables de Tiempos para control
unsigned long Time;
long Time_total = 20000;

//Tiempo de drenaje
const long drenaje = 8000;

long Time_delta = 0;
long Time_initial = 0;
long Time_final = 0;

float time_factor=1.2;

//Tiempo de espera para frecuancia de medición
int Time_Wait = 1000;


void setup() {

  pinMode(ledPin, OUTPUT);
  pinMode(CCW, OUTPUT);
  pinMode(CW, OUTPUT);
  pinMode(P, INPUT);

  Serial.begin(9600);
  digitalWrite(CCW, 0);
  digitalWrite(CW, 0);

  //INICIO DE SENSOR DE TEMPERATURA HTS
  //while (!Serial)
   // ;
  //Iniciación de Sensor HTS
  if (!HTS.begin()) {
    Serial.println("Failed to initialize humidity temperature sensor!");
    while (1)
      ;
  }

  //INICIO de Termocupla MAX31865
  //Serial.println("Adafruit MAX31865 PT100 Sensor Test!");
  thermo.begin(MAX31865_3WIRE);  // set to 2WIRE or 4WIRE as necessary
  //Inicio de vector temporal para temperatura y voltaje con valores de temperatura y voltaje de referencia
  for (int i = 0; i < 10; i++){
    temp_average_vector[i] = Temp_Ref;
  }
  for (int i = 0; i < 10; i++){
    V_average_vector[i] = V_Ref;
  }
  // INICIO DE RESET
  delay(1000);
  Serial.println("ICESTUPA CONTROL INICIANDO");
  delay(1000);
  //Reseta giro a la izquierda con delay de tiempo para asegurar su llegada al angulo de 90
  Reset();
  Time_initial = millis();

  /*Inicio de tarjeta SD
  //Serial.print("Initializing SD card...");

  // see if the card is present and can be initialized:
  //if (!SD.begin(CS1)) {
  //Serial.println("Card failed, or not present");
  // don't do anything more:
  //while (1);
  //}
  //Serial.println("card initialized.");
  */
}


////////////////////////////
//          LOOP          //
////////////////////////////

void loop() {

  Sensores();
  Voltaje();
  delay(Time_Wait);

}

////////////////////////////
//  FUNCIONES DE PROGRAMA //
////////////////////////////

//Función de medicion de los sensores del sistema, por lo que estan integrados los funcionales y se deberan integrar los futuros
void Sensores() {

  V_average();
  Temp_Int = HTS.readTemperature();
  Hum_Int = HTS.readHumidity();
  temp_average();
  Time_delta=map_m(V_Med,10,13,19250,14000);
  
  Serial.println("");
  Serial.print("Temp Interior= ");
  Serial.print(Temp_Int);
  Serial.println(" °C");
  /*Serial.print("Humedad Interior= ");
  //Serial.print(Hum_Int);
  //Serial.println(" %");
  //uint16_t rtd = thermo.readRTD();
  //Serial.print("RTD value: "); Serial.println(rtd);
  //float ratio = rtd;
  //ratio /= 32768;
  */
  Serial.print("Temp Exterior= ");
  Serial.print(Temp_Ext);
  Serial.println(" °C");
  Serial.print("Voltaje Convert= ");
  Serial.println(V_Med);
  Serial.print("Voltaje Medido= ");
  Serial.println(analogRead(S_Volt));
  
  /*
    String dataString = ""+String(Hum_Int)+";"+String(Temp_Int)+";"+String(Temp_Ext)+";"+String(Mode)+";"+String(analogRead(Spin_Active))+";"+String(analogRead(S_Volt));
    File dataFile = SD.open("ICESTUPACONTROL.txt", FILE_WRITE);
    // if the file is available, write to it:
    if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    // print to the serial port too:
    Serial.println(dataString);
    }
  */
}
//Función que permite del cambio de estado de la electrovalvula
void Voltaje() {
  if (V_Med < V_Ref-V_treshold) { // 10.8 V
      Vega();
      V_triggered = true;
  }
  else if (V_Med < V_Ref+V_treshold) // 11.4 V
  {
    if (V_triggered) // stay in Vega, wait for charging
      Vega();
    else
      Pulsador();
  }
  else // (V_Med > V_Ref+V_treshold){
  {
     Pulsador();
     V_triggered = false;
  }
  //else{
  //  Serial.println("Purgatorio");
  //}
}
//Función de activación de cambio de funcionamiento mediante el boton pulsador para realizar algun tipo de comprobación o trabajo sobre el sistema
void Pulsador() {
  if (digitalRead(P) == 1) {
    if (Mode == 0) {
      Serial.println(Mode);
      Vega();
      
    }
    if (Mode == 2) {
      Serial.println(Mode);
      Aspercion();
    }
  }
  if (digitalRead(P) == 0) {
    Temperatura();
  }
}
//Función de comparación de temperatura para iniciar las secuencias según las condiciones
void Temperatura() {
  //Serial.println("Temperatura");
  if (Temp_Ext < (Temp_Ref - Temp_treshold)) {
    Aspercion();
  }
  if (Temp_Ext > (Temp_Ref + Temp_treshold)) {
     Vega();
    }
  }


//Funcion aspersión según el estado de posición de la valvula y la demanda del sistema
void Aspercion() {
  Serial.println("Aspersion");
  if (Mode == 2) {
      Serial.println("A tirar agua");
      Giro_CW_T(Time_delta*time_factor);
      Mode = 0;
  }
  if (Mode == 0) {
    Serial.println("Tirando agua al ambiente");
    Giro_Stop();
  }
}
//Funcion para posicionar electrovalvula para recargar Humedal
void Vega() {
  Serial.println("Vega");
  if (Mode == 0) {
      Serial.println("A Drenar");
      Giro_CCW_T(Time_delta/2*time_factor);
      delay(drenaje);
      Serial.println("A la Vega");
      Giro_CCW_T(Time_delta*time_factor);
      Mode = 2;
    };
  if (Mode == 2) {
    Serial.println("Recargando Vega");
    Giro_Stop();
  };
}


///////////////////////////////////////
//        Funciones Básicas       /////
///////////////////////////////////////

float time_initial_ccw;
float time_final_ccw;
float time_initial_cw;
float time_final_cw;

//Función de giro de valvula contrahorario en función de un tiempo determinado tiempo de giro
void Giro_CCW_T(float tiempo_giro) {
  time_initial_ccw = millis();
  time_final_ccw = millis();
  //Serial.println("Giro_CCW_T");
  while (abs(time_initial_ccw - time_final_ccw) < tiempo_giro) {
    //Serial.println("Giro_CCW");
    //Serial.println(Mode);
    //Serial.println(tiempo_giro);
    //Serial.println(abs(time_initial_ccw - time_final_ccw));
    Giro_CCW();
    time_final_ccw = millis();
  };
  Giro_Stop();
}

//Función de giro de valvula horario en función de un tiempo determinado tiempo de giro
void Giro_CW_T(float tiempo_giro) {
  time_initial_cw = millis();
  time_final_cw = millis();
  //Serial.println("Giro_CW_T");
  while (abs(time_initial_cw - time_final_cw) < tiempo_giro) {
    //Serial.println("Giro_CW");
    //Serial.println(Mode);
    //Serial.println(tiempo_giro);
    //Serial.println(abs(time_initial_cw - time_final_cw));
    Giro_CW();
    time_final_cw = millis();
  };
  Giro_Stop();
}

//Función de giro de valvula en sentido contrahorario
void Giro_CCW() {

  CW_State = 0;
  CCW_State = 1;
  //Serial.println("Giro Antihorario ");
  //Serial.println("");
  digitalWrite(CW, CW_State);
  //Serial.print("CW = ");
  //Serial.print(CW_State);
  delay(250);
  digitalWrite(CCW, CCW_State);
  //Serial.print(" // CCW = ");
  //Serial.println(CCW_State);
  digitalWrite(ledPin, CW_State);
}

//Función de giro de valvula en sentido horario
void Giro_CW() {

  CW_State = 1;
  CCW_State = 0;
  //Serial.println("Giro Horario");
  //Serial.println("");
  digitalWrite(CCW, CCW_State);
  //Serial.print("CCW = ");
  //Serial.print(CCW_State);
  delay(250);
  digitalWrite(CW, CW_State);
  //Serial.print(" // CW = ");
  //Serial.println(CW_State);
  digitalWrite(ledPin, CW_State);
}

//Función de giro de valvula detenido
void Giro_Stop() {
  CW_State = 0;
  CCW_State = 0;
  //Serial.println("Giro Stop");
  //Serial.println("");
  digitalWrite(CW, CW_State);
  digitalWrite(CCW, CCW_State);
}

//Función para calcular el tiempo de apertura para llegar a punto medio
void Reset() {

  Serial.println(" ");
  Serial.println("Reset");
  Giro_CCW();
  delay(Time_total);
  //Configuración de valores de referencia cuando activaciones de giro estan en 0
  Spin_Ref0 = analogRead(Spin_Active);
  Serial.print(" Referencia 0 de Spin = ");
  Serial.println(Spin_Ref0);
  //delay(1000);

  //Determinación de diferencia de tiempo de giro en 180 grados
  Serial.println("Inicio de Reset Tiempo de Giro");
  Time_initial = millis();
  Giro_CW();
  delay(2000);
  while (analogRead(Spin_Active) > Spin_Ref0) {
    Giro_CW();
    Serial.print(" Spin = ");
    Serial.println(analogRead(Spin_Active));
    delay(100);
  };
  //Giro_Stop();
  Time_final = millis();
  Time_delta = abs(Time_final - Time_initial);
  Serial.print("Delta tiempo entre 90° y 270° = ");  //El estado de la válvula queda en 270° grados.
  Serial.println(Time_delta);
  delay(1000);
}

void temp_average(){
  temp_time();
  float sum = 0;
  for (int i = 0; i < 10; i++){
    sum = sum + temp_average_vector[i];
  }
  Temp_Ext = sum/10;
}

void temp_time(){
  for (int i = 0; i < 10; i++){
    temp_average_vector[i] = temp_average_vector[i+1];
  }
  temp_average_vector[9] = thermo.temperature(RNOMINAL, RREF);
}

void V_average(){
  V_time();
  float sum = 0;
  for (int i = 0; i < 10; i++){
    sum = sum + V_average_vector[i];
  }
  V_Med = sum/10;
}

float map_m(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void V_time(){
  for (int i = 0; i < 10; i++){
    V_average_vector[i] = V_average_vector[i+1];
  }
  V_average_vector[9] = map_m(analogRead(S_Volt),193,265,9.94,13.53);
}